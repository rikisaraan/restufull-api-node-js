'use strict'

var response = require('./res');
var connection = require('./conn');

exports.users = function (req, res) {
    connection.query("select * from users", function (error, rows, fields) {
        if (error) {
            console.log(error);
        } else {
            response.ok(rows, res)
        }
    });
};

exports.index = function (req, res) {
    response.ok("Hello from the node js restfull side ", res)
}